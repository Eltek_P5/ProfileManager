#include <Wire.h>
#include "LiquidCrystal_I2C.h"
#include <EEPROM.h>
#include "Bluetooth.h"
#include <SoftwareSerial.h>

//Lav en software serialport som kan kobles til computeren
SoftwareSerial computer(8, 7); //RX, TX

// set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 20, 4); 

//Deklarering af digitale input pins
const int modePin = 2; //Kontakten der skifter mellen "kør" og "juster PID"
const int defPin = 13; //Knappen der sætter PID-vørdierne til standard

//Deklarering af analoge input pins
const int joyInX = A0;
const int joyInY = A1;

//Deklarering af diverse variabler
int lcdCenter = 1; //Offset på displayet
int pid[3]; //Array med P, I, D, der starter på 0
int longDelay = 750; //En variabel der afgør hvor lang en stor mængde pause/forsinkelse er, deklareres
int shortDelay = 10; //En variabel der afgør hvor lang en kort mængde pause/forsinkelse er, deklareres
int vShortDelay = 1; //En variabel der afgør hvor lang en meget kort mængde pause/forsinkelse er, deklareres
int profile = 0; //profile variablen, der afgør hvilket profil der justeres, deklareres
int selection = 0; //selection variablen, der afgør om hh. P, I, eller D justeres, deklareres

int spacerEEP = 6; //En faktor, der forskyder placeringen af data EEPROM
int last = 0; //Bruges til knap debounce
int mode = 0; //Programmets status
float batPct = 0;

//Bluetooth instillinger
String baseModulAddr = "0006668C9AA7";



//Koden i setuppet bliver kørt én gang
void setup() {

    //Starter Serialle forbindelser
    Serial.begin(115200);
    computer.begin(9600);

    //LCDet startes
    lcd.init();
    lcd.backlight();

    //Pins bliver indstillet
    pinMode(modePin, INPUT_PULLUP);
    pinMode(defPin, INPUT_PULLUP);

    //Forbind bluetooth og vis loading bar
    lcd.clear();
    lcd.setCursor(4, 0);
    lcd.print("[#    ]");
    delay(100);
    connectToMaster(baseModulAddr);

    //Hent data fra EEPROM 
    getEEP();
}

//Funktion til at placere data i EEPROM
void putEEP () {
    EEPROM.put(0 * spacerEEP, pid[0]);
    EEPROM.put(1 * spacerEEP, pid[1]);
    EEPROM.put(2 * spacerEEP, pid[2]);
    pidSendAll(); //Sender alle PID-værdier til robotten
}

//Funktion til at hente data fra EEPROM
void getEEP () {
    EEPROM.get(0 * spacerEEP, pid[0]);
    EEPROM.get(1 * spacerEEP, pid[1]);
    EEPROM.get(2 * spacerEEP, pid[2]);
    drawMenu(); //Vis værdierne på displayet
    pidSendAll(); //send værdierne til robotten
}

//Nedenstående funktion sørger for at værdierne i arrayet ikke bliver hh. større-/mindre end 0 og 99
void pidValueCheck () {
    if (pid[selection] < 0) {
        pid[selection] = 99;
    }
    if (pid[selection] > 99) {
        pid[selection] = 0;
    }
}

//Sender den n'værdi 
void pidSend(int n) {
    switch (n) {
        case 0:
            Serial.write('p');
            Serial.write(pid[0]);
            break;
        case 1:
            Serial.write('i');
            Serial.write(pid[1]);
            break;
        case 2:
            Serial.write('d');
            Serial.write(pid[2]);
            break;
    }
}

//Hvis der ikke gives nogen n sender den den senest markerede PID-værdi
void pidSend(){
    pidSend(selection);
}

//Send alle værdier
void pidSendAll(){
    pidSend(0);
    pidSend(1);
    pidSend(2);
}

void drawMenu() {

    //Gør display klar, og sæt position
    lcd.clear();
    lcd.setCursor(lcdCenter, 0);

    //Afhængig af selection, skal displayet markere den rigtige
    switch (selection) {
        case 0:
            lcd.print(" P<   I    D");
            break;
        case 1:
            lcd.print(" P    I<   D");
            break;
        case 2:
            lcd.print(" P    I    D<");
            break;

    }

    //Den markerede PID-værdi vises centreres i henhold til den markerede værdi
    lcd.setCursor(lcdCenter + 1 + selection * 5, 1);
    lcd.print(pid[selection]);
}

//Koden i loopet bliver kørt gentagne gange
void loop() {

    //Læs fra knapperne
    int modeRead = digitalRead(modePin);
    int def = digitalRead(defPin);

    //Læs fra joystick
    int joyReadX = analogRead(joyInX);
    int joyReadY = analogRead(joyInY);

    //Hastighed ved ændring skal afhænge af hvor langt joysticket er skubbet
    int xDelay = abs(abs(joyReadX - 511) - 550);
    int yDelay = abs(abs(joyReadY - 511) * 0.5 - 500);
    int normalDelay = xDelay + yDelay;

    //Variablerne der afgør hvornår joysticket registreres som værende til ventstre/højre deklarerers lokalt
    int joyCutoffL = 403;
    int joyCutoffH = 603;

    //Variablerne der afgør standard PID-værdier deklareres lokalt
    int pDef = 60;
    int iDef = 15;
    int dDef = 60;

    //Koden herunder vil blive kørt hvert 750's millisekundt. 
    if (millis() - last > 750) {
        last = millis();
        //Checker om modeknappen har ændret sig, i de sidste 759 ms
            //Hvis ja skal den skifte mode 
        if (mode != modeRead) {
        lcd.clear();
            if (modeRead == 1) {
                putEEP();
            } else {
                getEEP();
                //Stop robotten
                Serial.write('s');

            }

        }
        mode = digitalRead(modePin);
    }

    //Hvis kontakten er til, skal der ske noget nyt
    if (mode == 0) {

        //Her forsinkes programmet lidt for at ikke at printe unødvendigt meget
        delay(shortDelay); 

        //Hvis joysticket er skubbet til den ene side på y-aksen, skal der ske noget nyt
        if (joyReadY >= joyCutoffH) {
            selection++;

            //selection sættes til 0, hvis den rammer 2, da der kun er 3 valg (P, I, D / 0, 1, 2) og for at skabe en "wrap around" effekt
            if (selection > 2) {
                selection = 0;
            }
            //Vent for at værdien ikke ændre sig for hurtigt
            delay(yDelay);
            drawMenu();


        }
        //Hvis joysticket er skubbet til den anden side på y-aksen, skal der ske noget nyt
        if (joyReadY <= joyCutoffL) {
            selection--;

            //Selection sættes til 2, hvis den rammer 0, da der kun er 3 valg (P, I, D / 0, 1, 2) og for at skabe en "wrap around" effekt
            if (selection < 0) {
                selection = 2;
            }
            delay(yDelay);
            drawMenu();

        }

        //Hvis joysticket er skubbet til den ene side på x-aksen, skal der ske noget nyt
        if (joyReadX >= joyCutoffH) {
            pid[selection]--;
            pidValueCheck (); //Checker at den ændrede værdi ikke for høj
            drawMenu(); //Viser det relevante på skærmen
            pidSend(); //Sender den relevante PID-værdi til robotten
            delay(xDelay);

        }
        //Hvis joysticket er skubbet til den anden side på x-aksen, skal der ske noget nyt
        if (joyReadX <= joyCutoffL) {
            pid[selection]++; //Der adderes én til den valgte værdi
            pidValueCheck (); //Checker at den ændrede værdi ikke for høj
            drawMenu(); //Viser det relevante på skærmen
            pidSend(); //Viser den relevante PID-værdi på skærmen
            delay(xDelay);
        }
        if (def == 0){
            pid[0] = pDef;
            pid[1] = iDef;
            pid[2] = dDef;
            drawMenu();
            pidSend();
        }
    } else {
/*
        if (Serial.available()) {
            byte batRead = Serial.read();
            int batPct = 100 - batRead;
            lcd.setCursor(lcdCenter, 1);
            String batText = "Robot B: ";
            batText += String(batPct);
            batText += " %";
            lcd.print(batText);


            float newBat = mapfloat(batRead, 0, 255, 0, 5) * 2.0;
            if(newBat != batPct){
                //Viser et stykke tekst der viser battery status på robotten
                lcd.setCursor(lcdCenter, 1);
                String batText = "Robot b: ";
                batText += String(newBat);
                batText += " V";
                lcd.print(batText);
            }
            batPct = newBat;

        }
        */

        //Vis på at robotten kører
        lcd.setCursor(lcdCenter, 0);
        lcd.print("Driving"); 


        //Hvis joysticket er skubbet i en retning, skal der ske noget nyt
        if (joyReadY <= 200) {
            Serial.write('h'); //Her sendes en besked til robotten om, at den skal køre fremad
        } else if (joyReadY >= 823) {
            Serial.write('v'); //Her sendes en besked til robotten om, at den skal køre bagud
        } else {
            Serial.write('l'); //Her sendes en besked til robotten om, at den skal køre lige ud
        }
        if (joyReadX <= 200) {
            //Her sendes en besked til robotten om, at den skal dreje til venstre
            Serial.write('f');
        } else if (joyReadX >= 823) {
            //Her sendes en besked til robotten om, at den skal dreje til højre
            Serial.write('t');
        } else {
            Serial.write('s');
        }
        //Her forsinkes programmet lidt for at ikke at printe unødvendigt meget
        delay(shortDelay);
    }


}

//////////////////////
//Taget fra Projekt 4
//////////////////////
bool connectToMaster(String addr) {

    //Gå ud af kommando mode for at være sikker
    Serial.println("---");
    delay(100);

    //Fortæl bruger hvad der sker.
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("[##   ]");

    //Da modulet vi vil forbinde til måske kan have en anden addresse kan man ændre det i toppen.
    String command = "C,";
    command += addr;

    //For at være sikker på at vores output læsning bliver korrekt tømmer vi serial bufferen, inden vi sender commandoen
    ReadAllSerial();
    runCommand(command, false);

    //Giv status på skærmen
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("[###  ]");


    //Her vil det være oplagt at lave en loading bar :-)
    delay(5000);

    //Gem outputtet i en String, som bare er en array med chars. Fordelen er at denne variabel type ændre størrelse af sig selv
    String output = ReadAllSerial();

    //Giv status, på skærmen
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("[#### ]");
    delay(250);

    //Hvis man har arbejdet med disse moduler ved man at de sender noget med "fail" i når de ikke kan forbinde
    if (output.indexOf("fail") != -1) {
        //Giv status på skærmen
        lcd.clear();
        lcd.setCursor(1, 0);
        lcd.print("Failed to connect");
        delay(250);
        return false;
    }
    return true;
}

float mapfloat(long x, long in_min, long in_max, long out_min, long out_max)
{
 return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
}
