//Det meste her er taget fra Projekt 4
#include "Bluetooth.h"

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////BLUETOOTH///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


//Taget fra Projekt 4
//Læser hele serielbufferen og returnere den
String ReadAllSerial() {
  //String kommer fra arduino, og giver mange smarte funktioner
  String out = "";
  //Kører sålængde der er seriel beskeder
  while (Serial.available()) {
    //Append string
    out += (char)Serial.read();
  }
  return out;
}

//Kører commando på bluetooth, hvis close er true vil den lukke cmd efter sig
void runCommand(String command, bool close) {
  //Start CMD
  Serial.print("$$$");
  delay(100);
  //Send commando
  Serial.println(command);

  //Luk hvis det er nødvendigt
  if (close) {
    delay(100);
    Serial.println("---");
  }
}
